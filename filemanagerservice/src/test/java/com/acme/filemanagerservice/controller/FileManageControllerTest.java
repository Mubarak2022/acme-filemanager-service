package com.acme.filemanagerservice.controller;

import com.acme.filemanagerservice.model.FileManagerRequest;
import com.acme.filemanagerservice.model.RestResponse;
import com.acme.filemanagerservice.service.FileManagerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
class FileManageControllerTest {
    static RestResponse restResponse = new RestResponse();
    static HttpServletRequest httpServletRequest;
    static ResponseEntity<byte[]> responseEntity;
    static FileManagerRequest fileManagerRequest = new FileManagerRequest();
    static ResponseEntity<String> responseEntityString;

    static MultipartFile mockMultipartFile;
    @Mock
    FileManagerService fileManagerService;
    @InjectMocks
    FileManageController fileManageController;

    @BeforeEach
    void setUp() {

        restResponse.setStatusCode(200);
        restResponse.setStatusMessage("successful");

        httpServletRequest = new MockHttpServletRequest();

        responseEntity = new ResponseEntity<>(HttpStatus.OK);
        responseEntityString = new ResponseEntity<>(HttpStatus.OK);

        fileManagerRequest.setFilePath("folder");
        fileManagerRequest.setDestinationFilePath("folder2");
        fileManagerRequest.setUserEmail("sam@gmail.com");

    }

    @Test
    void uploadFile() throws IOException {
        Mockito.when(fileManagerService.uploadFile(mockMultipartFile, "folder", httpServletRequest)).thenReturn(restResponse.getStatusMessage());
        assertEquals(restResponse.getStatusMessage(), fileManageController.uploadFile(mockMultipartFile, "folder", httpServletRequest));
    }

    @Test
    void getFile() throws IOException {
        Mockito.when(fileManagerService.getFile("folder", httpServletRequest)).thenReturn(responseEntity);
        assertEquals(responseEntity, fileManageController.getFile("folder", httpServletRequest));
    }

    @Test
    void deleteFile() throws IOException {
        Mockito.when(fileManagerService.deleteFile("folder", httpServletRequest)).thenReturn(responseEntityString);
        assertEquals(responseEntityString, fileManageController.deleteFile("folder", httpServletRequest));
    }

    @Test
    void cloneFile() throws IOException {
        Mockito.when(fileManagerService.cloneFile(fileManagerRequest, httpServletRequest)).thenReturn("done");
        assertNull(fileManageController.cloneFile("folder", "folder2", httpServletRequest));
    }
}