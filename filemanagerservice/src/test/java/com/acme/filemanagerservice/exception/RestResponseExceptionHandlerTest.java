package com.acme.filemanagerservice.exception;

import com.acme.filemanagerservice.model.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class RestResponseExceptionHandlerTest {


    static RestResponse restResponse = new RestResponse();
    @InjectMocks
    RestResponseExceptionHandler restResponseExceptionHandler;

    @BeforeEach
    void setUp() {
        restResponse.setStatusCode(400);
        restResponse.setStatusMessage("Exception");

    }

    @Test
    void handleResourceNotFoundException() {
        assertNotNull(restResponseExceptionHandler.handleResourceNotFoundException(new ResourceNotFoundException("Resource not found")));
    }

    @Test
    void handleCustomException() {
        assertNotNull(restResponseExceptionHandler.handleCustomException(new CustomException("Custom Exception")));

    }

    @Test
    void handleNoDataFoundException() {
        assertNotNull(restResponseExceptionHandler.handleNoDataFoundException(new NoDataFoundException("Data not found")));

    }

    @Test
    void handleHttpServerErrorException() {
        assertNotNull(restResponseExceptionHandler.handleHttpServerErrorException(new HttpServerErrorException(HttpStatus.BAD_REQUEST)));
    }

}