package com.acme.filemanagerservice.service;

import com.acme.filemanagerservice.entity.User;
import com.acme.filemanagerservice.repo.UserRep;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserAuthenticationServiceTest {
    static com.acme.filemanagerservice.entity.User user = new User();
    static UserDetails userDetails = new org.springframework.security.core.userdetails.User("34#Admin", "password", new ArrayList<>());
    @Mock
    UserRep userRep;
    @InjectMocks
    UserAuthenticationService userAuthenticationService;

    @BeforeEach
    void setUp() {
        user.setId(34);
        user.setName("sam");
        user.setEmail("sam@gmail.com");
        user.setPassword("password");
        user.setRole("Admin");


    }

    @Test
    void loadUserByUsername() {

        Mockito.when(userRep.findByEmail("sam@gmail.com")).thenReturn(user);
        assertEquals(userDetails, userAuthenticationService.loadUserByUsername("sam@gmail.com"));

    }

    @Test
    void loadUserByUserId() {

        Mockito.when(userRep.findById(34)).thenReturn(user);
        assertEquals(userDetails, userAuthenticationService.loadUserByUserId(34));

    }
}