package com.acme.filemanagerservice.exception;

import com.acme.filemanagerservice.model.RestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;

import static com.acme.filemanagerservice.util.FileMangerServiceConstant.*;

@ControllerAdvice
@Slf4j
public class RestResponseExceptionHandler {


    @ExceptionHandler
    public ResponseEntity<RestResponse> handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
        log.info(RESOURCE_NOT_FOUND_EXCEPTION, resourceNotFoundException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_FOUND.value(), resourceNotFoundException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleCustomException(CustomException customException) {
        log.info(CUSTOM_EXCEPTION, customException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), customException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleNoDataFoundException(NoDataFoundException noDataFoundException) {
        log.info(NO_DATA_FOUND_EXCEPTION, noDataFoundException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_FOUND.value(), noDataFoundException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleHttpServerErrorException(HttpServerErrorException httpServerErrorException) {
        log.info(HTTP_SERVER_ERROR_EXCEPTION, httpServerErrorException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.FORBIDDEN.value(), CURRENT_ACTION_IS_NOT_PERMISSIBLE), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({Exception.class})
    public ResponseEntity<RestResponse> handleGenericException(Exception exception) {
        log.info(GENERIC_EXCEPTION, exception);
        return new ResponseEntity<>(new RestResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getCause().getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
