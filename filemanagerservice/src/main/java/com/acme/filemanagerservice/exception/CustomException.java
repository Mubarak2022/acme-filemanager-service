package com.acme.filemanagerservice.exception;

public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }

}
