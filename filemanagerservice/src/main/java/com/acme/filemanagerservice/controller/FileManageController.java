package com.acme.filemanagerservice.controller;

import com.acme.filemanagerservice.model.FileManagerRequest;
import com.acme.filemanagerservice.service.FileManagerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/acme")
public class FileManageController {

    private final FileManagerService fileManagerService;

    public FileManageController(FileManagerService fileManagerService) {
        this.fileManagerService = fileManagerService;
    }

    @PutMapping("/document")
    public String uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("folderPath") String folderPath, HttpServletRequest request) throws IOException {

        return fileManagerService.uploadFile(file, folderPath, request);
    }

    @GetMapping("/document")
    public ResponseEntity<byte[]> getFile(@RequestParam("filePath") String filePath, HttpServletRequest request) throws IOException {

        return fileManagerService.getFile(filePath, request);
    }

    @DeleteMapping("/document")
    public ResponseEntity<String> deleteFile(@RequestParam("filePath") String filePath, HttpServletRequest request) throws IOException {

        return fileManagerService.deleteFile(filePath, request);
    }

    @PutMapping("/document/clone")
    public String cloneFile(@RequestParam("sourceFilePath") String sourceFilePath, @RequestParam("destinationFilePath") String destinationFilePath, HttpServletRequest request) throws IOException {

        FileManagerRequest fileManagerRequest = new FileManagerRequest();
        fileManagerRequest.setFilePath(sourceFilePath);
        fileManagerRequest.setDestinationFilePath(destinationFilePath);

        return fileManagerService.cloneFile(fileManagerRequest, request);
    }
}
