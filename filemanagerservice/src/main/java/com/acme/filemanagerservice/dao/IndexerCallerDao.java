package com.acme.filemanagerservice.dao;

import com.acme.filemanagerservice.model.IndexerRequest;
import com.acme.filemanagerservice.model.RestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Component
@Slf4j
public class IndexerCallerDao {
    private final RestTemplate restTemplate;

    public IndexerCallerDao(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RestResponse callIndexerService(IndexerRequest indexerRequest, HttpServletRequest httpServletRequest, HttpMethod httpMethod, String url) {
        indexerRequest.setDocument(true);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set("Authorization", httpServletRequest.getHeader("Authorization"));
        HttpEntity<IndexerRequest> request = new HttpEntity<>(indexerRequest, httpHeaders);
        log.info("callIndexerService indexerBaseUrl : " + url);
        RestResponse indexerResponse = restTemplate.exchange(url, httpMethod, request, RestResponse.class).getBody();
        log.info("callIndexerService Response : " + indexerResponse);

        return indexerResponse;
    }


}
