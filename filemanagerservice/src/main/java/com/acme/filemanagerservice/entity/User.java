package com.acme.filemanagerservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "Name")
    private String name;

    @NotNull
    @Column(name = "Email", unique = true, length = 200)
    private String email;

    @Column(name = "Password")
    private String password;

    @Column(name = "Role")
    private String role;


}
