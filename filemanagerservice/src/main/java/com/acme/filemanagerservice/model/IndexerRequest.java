package com.acme.filemanagerservice.model;

import lombok.*;

import javax.validation.constraints.NotNull;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IndexerRequest {
    @NotNull
    private boolean isDocument;
    @NotNull
    private String filePath;
    private boolean copyFlag;
    private Integer newParentFolderId;
    private boolean permanentDeleteFlag;
}
