package com.acme.filemanagerservice.model;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RestResponse {
    private Integer statusCode;
    private String statusMessage;
}
