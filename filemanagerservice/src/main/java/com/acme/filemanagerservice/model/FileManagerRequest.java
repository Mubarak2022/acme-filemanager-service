package com.acme.filemanagerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileManagerRequest {
    @NotNull
    private String userEmail;
    private String filePath;
    private String destinationFilePath;
}
