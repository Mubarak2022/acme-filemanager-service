package com.acme.filemanagerservice.repo;

import com.acme.filemanagerservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRep extends JpaRepository<User, String> {
    User findByEmail(String email);

    User findById(Integer userId);

    @Query("Select id from User u where u.email = ?1")
    String getUserIdByEmail(String email);

    @Transactional
    @Modifying
    @Query("delete from User u where u.email = ?1")
    int deleteByEmail(String email);


}
