package com.acme.filemanagerservice.service;

import com.acme.filemanagerservice.model.FileManagerRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public interface FileManagerService {

    String uploadFile(MultipartFile file, String folderPath, HttpServletRequest request) throws IOException;

    ResponseEntity<byte[]> getFile(String filePath, HttpServletRequest request) throws IOException;

    ResponseEntity<String> deleteFile(String filePath, HttpServletRequest request) throws IOException;

    String cloneFile(FileManagerRequest fileManagerRequest, HttpServletRequest request) throws IOException;


}
