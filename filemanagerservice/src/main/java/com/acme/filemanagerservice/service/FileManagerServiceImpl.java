package com.acme.filemanagerservice.service;

import com.acme.filemanagerservice.dao.IndexerCallerDao;
import com.acme.filemanagerservice.exception.CustomException;
import com.acme.filemanagerservice.model.FileManagerRequest;
import com.acme.filemanagerservice.model.IndexerRequest;
import com.acme.filemanagerservice.model.RestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static com.acme.filemanagerservice.util.FileMangerServiceConstant.*;

@Service
@Slf4j
public class FileManagerServiceImpl implements FileManagerService {


    private final IndexerCallerDao indexerCallerDao;
    @Value("${file.base.path}")
    private String fileBasePath;

    @Value("${indexer.service.base.url}")
    private String indexerBaseUrl;

    public FileManagerServiceImpl(IndexerCallerDao indexerCallerDao) {
        this.indexerCallerDao = indexerCallerDao;
    }

    private static void indexerServiceResponseCheck(RestResponse indexerResponse) {
        if (indexerResponse.getStatusCode() != 200) {
            throw new CustomException("Internal server error");
        }

    }

    @Override
    public String uploadFile(MultipartFile file, String folderPath, HttpServletRequest request) throws IOException {
        String name = file.getOriginalFilename();


        IndexerRequest indexerRequest = new IndexerRequest();
        indexerRequest.setDocument(true);


        if (name != null)
            indexerRequest.setFilePath(folderPath + "/" + name);
        else
            return "failed Task, file name is missing";

        log.info("indexerRequest : " + indexerRequest);
        RestResponse indexerResponse = indexerCallerDao.callIndexerService(indexerRequest, request, HttpMethod.POST, indexerBaseUrl);
        indexerServiceResponseCheck(indexerResponse);
        String filename = indexerResponse.getStatusMessage();
        log.info("indexerResponse : " + indexerResponse);
        String filePath = fileBasePath + filename;
        file.transferTo(new File(filePath));
        return FILE_UPLOADED_SUCCESSFULLY;

    }

    @Override
    public ResponseEntity<byte[]> getFile(String entityFilePath, HttpServletRequest request) throws IOException {


        RestResponse indexerResponse = indexerCallerDao.callIndexerService(new IndexerRequest(), request, HttpMethod.GET, indexerBaseUrl
                + FILE_PATH + entityFilePath);

        indexerServiceResponseCheck(indexerResponse);
        String fileId = indexerResponse.getStatusMessage();
        log.info("getFile indexerResponse : " + indexerResponse);

        String filePath = fileBasePath + fileId;
        log.info("getFile fileId : " + fileId);

        byte[] fileData = Files.readAllBytes(new File(filePath).toPath());
        return ResponseEntity.status(HttpStatus.OK)
                .body(fileData);
    }

    @Override
    public ResponseEntity<String> deleteFile(String entityFilePath, HttpServletRequest request) throws IOException {

        IndexerRequest indexerRequest = new IndexerRequest();
        indexerRequest.setFilePath(entityFilePath);
        RestResponse indexerResponse = indexerCallerDao.callIndexerService(indexerRequest, request, HttpMethod.GET, indexerBaseUrl
                + FILE_PATH + indexerRequest.getFilePath());
        indexerServiceResponseCheck(indexerResponse);
        String fileId = indexerResponse.getStatusMessage();
        log.info("deleteFile indexerResponse : " + indexerResponse);
        String filePath = fileBasePath + fileId;
        log.info("deleteFile fileId : " + fileId);

        indexerRequest = new IndexerRequest();
        indexerRequest.setFilePath(entityFilePath);
        indexerRequest.setPermanentDeleteFlag(true);
        RestResponse metaDataResponse = indexerCallerDao.callIndexerService(indexerRequest, request, HttpMethod.DELETE, indexerBaseUrl);
        log.info("deleteFile metaDataResponse : " + metaDataResponse);
        if (metaDataResponse.getStatusCode() == 200) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(Files.deleteIfExists(new File(filePath).toPath())
                            ? "File Deleted Successfully"
                            : "File does not exist");
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(metaDataResponse.getStatusMessage());
    }

    @Override
    public String cloneFile(FileManagerRequest fileManagerRequest, HttpServletRequest request) throws IOException {
        IndexerRequest indexerRequest = new IndexerRequest();
        indexerRequest.setFilePath(fileManagerRequest.getFilePath());
        RestResponse indexerResponse = indexerCallerDao.callIndexerService(indexerRequest, request, HttpMethod.GET, indexerBaseUrl + FILE_PATH + indexerRequest.getFilePath());

        indexerServiceResponseCheck(indexerResponse);
        String sourceFileId = indexerResponse.getStatusMessage();

        indexerRequest = new IndexerRequest();
        indexerRequest.setDocument(true);
        indexerRequest.setFilePath(fileManagerRequest.getDestinationFilePath());
        log.info("cloneFile indexerRequest : " + indexerRequest);


        RestResponse indexerPostResponse = indexerCallerDao.callIndexerService(indexerRequest, request, HttpMethod.POST, indexerBaseUrl);
        String destinationFileId = indexerPostResponse.getStatusMessage();
        log.info("cloneFile indexerResponse : " + indexerPostResponse);
        indexerServiceResponseCheck(indexerResponse);
        String sourceUrl = fileBasePath + sourceFileId;
        String destinationUrl = fileBasePath + destinationFileId;

        Files.copy(
                new File(sourceUrl).toPath(),
                new File(destinationUrl).toPath());

        return FILE_SUCCESSFULLY_CLONED;
    }


}
