package com.acme.filemanagerservice.service;


import com.acme.filemanagerservice.repo.UserRep;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserAuthenticationService implements UserDetailsService {
    private final UserRep userRep;

    public UserAuthenticationService(UserRep userRep) {
        this.userRep = userRep;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        com.acme.filemanagerservice.entity.User user = userRep.findByEmail(email);
        if (user != null) {
            return new User(user.getId() + "#" + user.getRole(), user.getPassword(), new ArrayList<>());
        }

        return null;
    }

    public UserDetails loadUserByUserId(Integer userId) throws UsernameNotFoundException {

        com.acme.filemanagerservice.entity.User user = userRep.findById(userId);
        if (user != null) {
            return new User(user.getId() + "#" + user.getRole(), user.getPassword(), new ArrayList<>());
        }

        return null;
    }

}
