package com.acme.filemanagerservice.util;

import com.acme.filemanagerservice.model.UserDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.function.Function;

@Component
@Slf4j
public class JWTUtility implements Serializable {

    private static final long serialVersionUID = 234234523523L;
    @Value("${jwt.secret}")
    private String secretKey;

    //retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }


    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieving any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }


    //check if the token has expired
    private boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }


    //validate token
    public boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    //retrieve user email and Role from jwt token
    public UserDTO getUserDataFromToken(HttpServletRequest request) {
        UserDTO userDTO = new UserDTO();
        String authorization = request.getHeader(FileMangerServiceConstant.AUTHORIZATION);
        String token = null;
        String tokenSubject;
        if (null != authorization && authorization.startsWith(FileMangerServiceConstant.MTOKEN)) {
            token = authorization.substring(7);
            tokenSubject = getUsernameFromToken(token);
            if (tokenSubject != null) {
                userDTO.setUserId(Integer.parseInt(tokenSubject.split("#")[0]));
                userDTO.setRole(tokenSubject.split("#")[1]);
                log.info("userDTO :{}", userDTO);
            }
        }
        return userDTO;
    }
}