package com.acme.filemanagerservice.util;

import java.io.Serializable;

public final class FileMangerServiceConstant implements Serializable {
    public static final String RESOURCE_NOT_FOUND_EXCEPTION = "ResourceNotFoundException : {}";
    public static final String CUSTOM_EXCEPTION = "CustomException : {}";
    public static final String NO_DATA_FOUND_EXCEPTION = "NoDataFoundException : {}";
    public static final String HTTP_SERVER_ERROR_EXCEPTION = "HttpServerErrorException : {}";
    public static final String METHOD_ARGUMENT_NOT_VALID_EXCEPTION = "MethodArgumentNotValidException : {}";
    public static final String GENERIC_EXCEPTION = "GenericException : {}";
    public static final String THIS_OPERATION_IS_NOT_ALLOWED = "This Operation is not allowed";
    public static final String VALID_FIELD_MISSING = "Valid Field Missing";
    public static final String CURRENT_ACTION_IS_NOT_PERMISSIBLE = "Current action is not permissible";
    public static final String AUTH_WHITELIST_VALUE1 = "/acme/indexer/**";
    public static final String AUTH_WHITELIST_VALUE2 = "/v3/api-docs/**";
    public static final String AUTH_WHITELIST_VALUE3 = "/swagger-ui/**";
    public static final String AUTH_WHITELIST_VALUE4 = "/acme/view";
    public static final String AUTH_WHITELIST_VALUE5 = "/acme/role";

    public static final String AUTHORIZATION = "Authorization";
    public static final String MTOKEN = "MToken ";

    public static final String FILE_UPLOADED_SUCCESSFULLY = "File Uploaded successfully.";
    public static final String FILE_SUCCESSFULLY_CLONED = "file successfully Cloned";
    public static final String FILE_PATH = "?filePath=";
}
